import javax.swing.JOptionPane;

public class Principal
{
	public static void main (String arg [])
	{
		int dato=1;
		Cliente cliente;
		cliente= new Cliente ();
		//dato= 1;
		
		while (dato !=7)
		{
			dato=Integer.parseInt (JOptionPane.showInputDialog ("******Diversion a otro Nivel******"
				+"\n1. Ingresar Datos del Cliente"
				+"\n2. Modificar Altura del Cliente"
				+"\n3. Modificar el Peso del Cliente"
				+"\n4. La Altura del Cliente es mayor a 1.8 metros?"
				+"\n5. Determinar las Atracciones que el Cliente puede Disfrutar"
				+"\n6. Mostrar Datos del Cliente"
				+"\n7. Salir"
				+"\n "
				+"\n Por favor escoja una opcion:"));
				switch (dato)
				{
					case 1:
						String nombre, identificacion;
						int edad, peso;
						double altura;
						identificacion=JOptionPane.showInputDialog ("Digite la identificación del cliente:");
						cliente.setIdentificacion (identificacion);
						nombre=JOptionPane.showInputDialog ("Digite el nombre del cliente:");
						cliente.setNombre (nombre);
						edad=Integer.parseInt(JOptionPane.showInputDialog ("Digite la edad del cliente:"));
						cliente.setEdad(edad);
						peso=Integer.parseInt(JOptionPane.showInputDialog ("Digite el peso del cliente:"));
						cliente.setPeso(peso);
						altura=Double.parseDouble(JOptionPane.showInputDialog ("Digite la altura del cliente:"));
						cliente.setAltura(altura);
					break;
					
					case 2:
						int opcionM;
						opcionM=0;
						opcionM=JOptionPane.showConfirmDialog(null,"¿Seguro que desea modificar la altura del cliente?","Modificar",JOptionPane.YES_NO_OPTION);
						if (opcionM==0){
							altura=Integer.parseInt(JOptionPane.showInputDialog ("Digite la altura de cliente:"));
							cliente.setAltura(altura);
						}//Fin del if
						JOptionPane.showMessageDialog (null, "La altura del cliente ha sido modificada");}
					break;
					
					case 3:
						int opcionMP;
						opcionMP=0;
						opcionMP=JOptionPane.showConfirmDialog(null,"¿Seguro que desea modificar el peso del cliente?","Modificar",JOptionPane.YES_NO_OPTION);
						if (opcionMP==0){
							peso=Integer.parseInt(JOptionPane.showInputDialog ("Digite el peso del cliente:"));
							cliente.setPeso(peso);
						}//Fin del if
						JOptionPane.showMessageDialog (null, "El peso del cliente ha sido modificado");}
					break;
					
					case 4: JOptionPane.showMessageDialog (null, "El cliente tiene una altura mayor a 1.8?"+"\n"+ cliente.evaluarAltura ());
					break;
					
					case 5: JOptionPane.showMessageDialog (null, "Las atracciones a las que el cliente se puede subir son"+"\n"+ cliente.evaluarAtracciones ());
					break;
					
					case 6: JOptionPane.showMessageDialog (null, "Los datos del cliente son los siguientes:"+"\n"+ cliente.toString ());
					break;
					
					case 7: JOptionPane.showMessageDialog (null,"Nos vemos, regresa pronto");
					break;
					
					default:{JOptionPane.showMessageDialog (null,"Esa opcion no es valida");}
				}//Fin del switch
		}//Fin del While
	}//Fin del main
}//Fin de la clase Registro
